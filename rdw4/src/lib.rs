pub use gtk;

mod content_provider;
mod display;
mod egl;
mod error;
mod gstaudio;
mod keymap;
mod usbredir;
#[cfg(all(windows, not(feature = "bindings")))]
mod win32;

#[cfg(not(feature = "bindings"))]
mod paintable;
#[cfg(not(feature = "bindings"))]
mod picture;

pub use content_provider::ContentProvider;
pub use display::*;
#[cfg(windows)]
pub use egl::RdwD3d11Texture2dScanout;
#[cfg(unix)]
pub use egl::RdwDmabufScanout;
pub use error::Error;
pub use gstaudio::*;
pub use keymap::*;
pub use usbredir::{Device as UsbDevice, UsbRedir};

#[cfg(feature = "capi")]
mod capi;

mod ffi;

mod enums;
pub use enums::*;

mod logger;
pub use logger::*;
